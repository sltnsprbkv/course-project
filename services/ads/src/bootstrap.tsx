import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import App from './components/App/App'

const root = document.getElementById('root')

if (!root) {
	throw new Error('Root is undefined!')
}

const container = createRoot(root)
container.render(
	<StrictMode>
		<App />
	</StrictMode>
)
