import { Outlet, useLocation } from 'react-router-dom'
import Header from '../Header/Header'
import styles from './App.module.scss'

export const App = () => {
	const { pathname } = useLocation()

	return (
		<div className={styles.container}>
			<Header />
			{pathname === '/' && (
				<h1 className={styles.banner}>
					Welcome! <br /> This is a HOST
				</h1>
			)}
			<Outlet />
		</div>
	)
}
