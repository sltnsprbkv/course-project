import { cartRoutes, shopRoutes } from '@packages/shared'
import { Link, useLocation } from 'react-router-dom'
import styles from './Header.module.scss'

export default function Header() {
	const { pathname } = useLocation()

	return (
		<header className={styles.header}>
			<Link to='/' className={styles.header__logo}>
				ShopRoom
			</Link>
			<nav className={styles.header__nav}>
				<Link
					to={shopRoutes.main}
					className={
						pathname == shopRoutes.main ? styles.header__nav__active : ''
					}
				>
					Shop
				</Link>
				<Link
					to={cartRoutes.details}
					className={
						pathname == cartRoutes.details ? styles.header__nav__active : ''
					}
				>
					Cart
				</Link>
			</nav>
		</header>
	)
}
