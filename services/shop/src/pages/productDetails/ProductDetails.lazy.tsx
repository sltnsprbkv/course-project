import { lazy } from 'react'

export const ProductDetailsPage = lazy(() => import('./ProductDetails'))
