import React, {
	ReactNode,
	createContext,
	useContext,
	useEffect,
	useState,
} from 'react'

interface Product {
	id: string
	category: string
	name: string
	price: number
	imgSrc: string
	isAddCart: boolean
	content: string
}

interface CartContextProps {
	productList: Product[]
	cartList: Product[]
	fetchProductList: () => Promise<void>
	fetchProductDetails: (id: string) => Promise<Product | undefined>
	handleAddToCart: (id: string, isAddCart: boolean) => Promise<void>
}

const CartContext = createContext<CartContextProps | undefined>(undefined)

export const useCart = (): CartContextProps => {
	const context = useContext(CartContext)
	if (!context) {
		throw new Error('useCart must be used within a CartProvider')
	}
	return context
}

interface CartProviderProps {
	children: ReactNode
}

export const CartProvider: React.FC<CartProviderProps> = ({ children }) => {
	const [productList, setProductList] = useState<Product[]>([])
	const [cartList, setCartList] = useState<Product[]>([])

	const fetchProductList = async () => {
		try {
			const response = await fetch('http://localhost:8888/products')
			if (!response.ok) {
				throw new Error('Network response was not ok')
			}
			const data: Product[] = await response.json()
			setProductList(data)
			setCartList(data.filter(item => item.isAddCart))
		} catch (error) {
			console.error('Error fetching data:', error)
		}
	}

	const fetchProductDetails = async (
		id: string
	): Promise<Product | undefined> => {
		try {
			const response = await fetch(`http://localhost:8888/products/${id}`)
			if (!response.ok) {
				throw new Error('Network response was not ok')
			}
			const product: Product = await response.json()
			return product
		} catch (error) {
			console.error('Error fetching product details:', error)
			return undefined
		}
	}

	const handleAddToCart = async (id: string, isAddCart: boolean) => {
		try {
			const response = await fetch(`http://localhost:8888/products/${id}`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({ isAddCart }),
			})

			if (!response.ok) {
				throw new Error('Network response was not ok')
			}

			await response.json()
			fetchProductList()
		} catch (error) {
			console.error('There was a problem with your fetch operation:', error)
		}
	}

	useEffect(() => {
		fetchProductList()
	}, [])

	return (
		<CartContext.Provider
			value={{
				productList,
				cartList,
				fetchProductList,
				fetchProductDetails,
				handleAddToCart,
			}}
		>
			{children}
		</CartContext.Provider>
	)
}
