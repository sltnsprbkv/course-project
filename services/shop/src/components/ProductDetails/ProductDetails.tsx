import { useCart } from '@/context/CartContext'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import styles from './ProductDetails.module.scss'

export default function ProductDetails() {
	const { id } = useParams<{ id: string }>()
	const [product, setProduct] = useState(undefined)
	const [quantity, setQuantity] = useState(1)
	const { fetchProductDetails, handleAddToCart } = useCart()

	const handleIncreaseQuantity = () => {
		setQuantity(prevQuantity => prevQuantity + 1)
	}

	const handleDecreaseQuantity = () => {
		setQuantity(prevQuantity => Math.max(1, prevQuantity - 1))
	}

	const handleCartAction = () => {
		handleAddToCart(id, !product.isAddCart)
	}

	useEffect(() => {
		fetchProductDetails(id).then(data => setProduct(data))
	}, [id, handleCartAction])

	if (!product) {
		return <div>Loading...</div>
	}

	return (
		<section className={styles.container}>
			<div className={styles.productSection}>
				<div className={styles.productImage}>
					<img src={product.imgSrc} alt={product.name} />
				</div>
				<div className={styles.productDetails}>
					<h2 className={styles.productBrand}>BRAND NAME</h2>
					<h1 className={styles.productTitle}>{product.name}</h1>
					<h2 className={styles.productCategory}>
						Category: {product.category}
					</h2>
					<p className={styles.productDescription}>{product.content}</p>
					<div className={styles.productActions}>
						<span className={styles.productPrice}>${product.price}</span>
						<div className={styles.productActions__inner}>
							<div className={styles['quantity']}>
								<button
									className={styles.minus}
									aria-label='Decrease'
									onClick={handleDecreaseQuantity}
								>
									&minus;
								</button>
								<h2 className={styles.quantity__amount}>{quantity}</h2>
								<button
									className={styles.plus}
									aria-label='Increase'
									onClick={handleIncreaseQuantity}
								>
									+
								</button>
							</div>
							<button className={styles.addToCart} onClick={handleCartAction}>
								{product.isAddCart ? 'Удалить из корзины' : 'Добавить в корзину'}
							</button>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}
