import { useCart } from '@/context/CartContext'
import { ProductCard } from '../ProductCard/ProductCard'
import styles from './ProductList.module.scss'

export function ProductList() {
	const { productList, handleAddToCart } = useCart()

	return (
		<section className={styles.section}>
			{productList.map(product => (
				<ProductCard
					key={product.id}
					id={product.id}
					imgSrc={product.imgSrc}
					category={product.category}
					name={product.name}
					price={product.price}
					isAddCart={product.isAddCart}
					handleAddToCart={handleAddToCart}
				/>
			))}
		</section>
	)
}
