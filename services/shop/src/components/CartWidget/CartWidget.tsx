import { useCart } from '@/context/CartContext'
import { cartRoutes } from '@packages/shared'
import { Link } from 'react-router-dom'
import styles from './CartWidget.module.scss'

export default function CartWidget() {
	const { cartList, handleAddToCart } = useCart()

	return (
		<div className={styles.container}>
			{cartList.length > 0 ? (
				cartList.map(cart => (
					<div className={styles.card}>
						<span className={styles.card__info__details}>
							<h3>{cart.category}</h3>
							<h2>{cart.name}</h2>
							<p>${cart.price}</p>
						</span>
						<button
							className={styles.card__info__button}
							onClick={() => handleAddToCart(cart.id, !cart.isAddCart)}
						>
							Удалить
						</button>
					</div>
				))
			) : (
				<h1 className={styles.banner}>Пустая корзина</h1>
			)}
			<Link to={cartRoutes.details}>Перейти в корзину</Link>
		</div>
	)
}
