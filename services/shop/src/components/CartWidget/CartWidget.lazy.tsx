import { lazy } from 'react'

export const CartWidgetShop = lazy(() => import('./CartWidget'))
