import { Link } from 'react-router-dom'
import styles from './ProductCard.module.scss'

interface ProductCardProps {
	id: string
	name: string
	category: string
	price: number
	imgSrc: string
	isAddCart: boolean
	handleAddToCart: (id: string, isAddCart: boolean) => void
}

export function ProductCard({
	id,
	imgSrc,
	category,
	name,
	price,
	isAddCart,
	handleAddToCart,
}: ProductCardProps) {
	// TODO: add to details page with uuid
	return (
		<div className={styles.card}>
			<Link to={`/shop/products/${id}`} className={styles.card__image}>
				<img alt={`${name}-${imgSrc}`} src={imgSrc} />
			</Link>
			<div className={styles.card__info}>
				<h3>{category}</h3>
				<div className={styles.card__info__footer}>
					<span className={styles.card__info__details}>
						<Link to={`/shop/products/${id}`}>
							<h2>{name}</h2>
						</Link>
						<p>${price}</p>
					</span>
					<span>
						<button
							className={styles.card__info__button}
							onClick={() => handleAddToCart(id, !isAddCart)}
						>
							{isAddCart ? 'Удалить из корзины' : 'Добавить в корзину'}
						</button>
					</span>
				</div>
			</div>
		</div>
	)
}
