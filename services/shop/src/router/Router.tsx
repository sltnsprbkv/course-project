import { App } from '@/components/App/App'
import { CartProvider } from '@/context/CartContext'
import { ShopLayout } from '@/layouts/Layout'
import { LazyShop, ProductDetailsPage } from '@/pages'
import { Suspense } from 'react'
import { createBrowserRouter } from 'react-router-dom'

const routes = [
	{
		path: '/shop',
		element: (
			<CartProvider>
				<App />
			</CartProvider>
		),
		children: [
			{
				path: 'products',
				element: (
					<ShopLayout>
						<Suspense fallback={'Is loading'}>
							<LazyShop />
						</Suspense>
					</ShopLayout>
				),
			},
			{
				path: 'products/:id',
				element: (
					<ShopLayout>
						<Suspense fallback={'Is loading'}>
							<ProductDetailsPage />
						</Suspense>
					</ShopLayout>
				),
			},
		],
	},
]

export const router = createBrowserRouter(routes)
export default routes
