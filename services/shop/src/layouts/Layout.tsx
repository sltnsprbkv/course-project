import { CartWidgetShop } from '@/components/CartWidget/CartWidget.lazy'
import React, { Suspense } from 'react'
import styles from './Layouts.module.scss'
const Ads = React.lazy(
	() => import('ads@http://localhost:3003/remoteEntry.js/Module')
)

export function ShopLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className={styles.wrapper}>
			{children}
			<div className={styles.left_sidebar}>
				<Suspense fallback={<div>Is loading...</div>}>
					<Ads />
				</Suspense>
				<Suspense fallback={<div>Is loading...</div>}>
					<CartWidgetShop />
				</Suspense>
			</div>
		</div>
	)
}
