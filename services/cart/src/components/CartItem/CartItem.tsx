import styles from './CartItem.module.scss'

interface CartItemProps {
	id: string
	name: string
	category: string
	price: number
	imgSrc: string
	isAddCart: boolean
	handleAddToCart: (id: string, isAddCart: boolean) => void
}

export default function CartItem({
	id,
	imgSrc,
	category,
	name,
	price,
	isAddCart,
	handleAddToCart,
}: CartItemProps) {
	return (
		<div className={styles.card}>
			<a className={styles.card__image}>
				<img alt={`${name}-${imgSrc}`} src={imgSrc} />
			</a>
			<div className={styles.card__info}>
				<h3>{category}</h3>
				<div className={styles.card__info__footer}>
					<span className={styles.card__info__details}>
						<h2>{name}</h2>
						<p>${price}</p>
					</span>
					<span>
						<button
							className={styles.card__info__button}
							onClick={() => handleAddToCart(id, !isAddCart)}
						>
							{isAddCart ? 'Удалить из корзины' : 'Добавить в корзину'}
						</button>
					</span>
				</div>
			</div>
		</div>
	)
}
