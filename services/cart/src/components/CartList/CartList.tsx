import { useEffect, useState } from 'react'
import CartItem from '../CartItem/CartItem'
import styles from './CartList.module.scss'

export default function CartList() {
	const [cartList, setCartList] = useState([])

	const fetchCartList = async () => {
		try {
			const response = await fetch('http://localhost:8888/products')
			if (!response.ok) {
				throw new Error('Network response was not ok')
			}
			const data = await response.json()
			setCartList(data.filter((item: { isAddCart: boolean }) => item.isAddCart))
		} catch (error) {
			console.error('Error fetching data:', error)
		}
	}

	const handleAddToCart = async (id: string, isAddCart: boolean) => {
		try {
			const response = await fetch(`http://localhost:8888/products/${id}`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					isAddCart,
				}),
			})

			if (!response.ok) {
				throw new Error('Network response was not ok')
			}

			await response.json()
			fetchCartList()
		} catch (error) {
			console.error('There was a problem with your fetch operation:', error)
		}
	}

	useEffect(() => {
		fetchCartList()
	}, [])
	return (
		<div className={styles.container}>
			{cartList.length > 0 ? (
				cartList.map(cart => (
					<CartItem
						key={cart.id}
						id={cart.id}
						imgSrc={cart.imgSrc}
						category={cart.category}
						name={cart.name}
						price={cart.price}
						isAddCart={cart.isAddCart}
						handleAddToCart={handleAddToCart}
					/>
				))
			) : (
				<h1 className={styles.banner}>Пустая корзина</h1>
			)}
		</div>
	)
}
