import { App } from '@/components/App/App'
import { LazyAbout } from '@/pages'
import { Suspense } from 'react'
import { createBrowserRouter } from 'react-router-dom'

const routes = [
	{
		path: '/cart',
		element: <App />,
		children: [
			{
				path: '/cart/details',
				element: (
					<Suspense fallback={'Is loading'}>
						<LazyAbout />
					</Suspense>
				),
			},
		],
	},
]

export const router = createBrowserRouter(routes)
export default routes
