export const shopRoutes = {
	main: '/shop/products',
	productsDetails: '/shop/products/:id',
}
